import xml.etree.ElementTree as ET

import requests
import yaml


class IndentDumper(yaml.Dumper):
    def increase_indent(self, flow=False, indentless=False):
        return super(IndentDumper, self).increase_indent(flow, False)


metadata_url = "https://metadata.eduid.cz/entities/eduid+idp"

categories = {
    "http://eduid.cz/uri/idp-group/university": [
        "employee",
        "faculty",
        "member",
        "student",
        "staff",
    ],
    "http://eduid.cz/uri/idp-group/avcr": ["member"],
    "http://eduid.cz/uri/idp-group/library": ["employee"],
    "http://eduid.cz/uri/idp-group/hospital": ["employee"],
    "http://eduid.cz/uri/idp-group/other": ["employee", "member"],
}

attr_name = "http://macedir.org/entity-category"

exception_datafile = "einfra_isacademic/exceptions_data.yaml"
output_file = "einfra_isacademic/result.yaml"

xml_key_attr_value = "{urn:oasis:names:tc:SAML:2.0:assertion}AttributeValue"
xml_key_entity_attributes = (
    "{urn:oasis:names:tc:SAML:metadata:attribute}EntityAttributes"
)
xml_key_attribute = "{urn:oasis:names:tc:SAML:2.0:assertion}Attribute"

metadata_ns = "{urn:oasis:names:tc:SAML:2.0:metadata}"

res = {}
exception_data = {}

resp = requests.get(metadata_url)
metadata = ET.fromstring(resp.content)

with open(exception_datafile) as f:
    documents = yaml.full_load(f)

    if documents is not None:
        for item, doc in documents.items():
            exception_data[item] = doc

for child in metadata.findall(metadata_ns + "EntityDescriptor"):
    entity_id = child.get("entityID")
    if entity_id in exception_data.keys():
        continue

    entity_attributes_objs = child.find(metadata_ns + "Extensions").findall(
        xml_key_entity_attributes
    )

    for entity_attribute in entity_attributes_objs:
        attrs = entity_attribute.findall(xml_key_attribute)
        allowed_affiliations = []
        for attr in attrs:
            if attr.get("Name") == attr_name:
                for category in attr.findall(xml_key_attr_value):
                    if category.text in categories.keys():
                        allowed_affiliations = categories[category.text]
        res[entity_id] = allowed_affiliations

for entity_id, allowed_affiliations in exception_data.items():
    res[entity_id] = allowed_affiliations

with open(output_file, "w") as file:
    documents = yaml.dump(res, file, Dumper=IndentDumper, allow_unicode=True)
