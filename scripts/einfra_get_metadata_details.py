import os
import xml.etree.ElementTree as ET

import requests
import yaml


class IndentDumper(yaml.Dumper):
    def increase_indent(self, flow=False, indentless=False):
        return super(IndentDumper, self).increase_indent(flow, False)


metadata_url = "https://metadata.eduid.cz/entities/eduid+idp"

metadata_ns = "{urn:oasis:names:tc:SAML:2.0:metadata}"

output_dir = "einfra_metadata_details"
output_file = output_dir + "/result.yaml"

res = {}

resp = requests.get(metadata_url)
metadata = ET.fromstring(resp.content)

for child in metadata.findall(metadata_ns + "EntityDescriptor"):
    entity_id = child.get("entityID")

    idp_data = {}

    scopes_obj = (
        child.find(metadata_ns + "IDPSSODescriptor")
        .find(metadata_ns + "Extensions")
        .findall("{urn:mace:shibboleth:metadata:1.0}Scope")
    )

    scopes = []

    if scopes_obj is not None:
        for item in scopes_obj:
            scope = item.text
            scopes.append(scope)

    organizations = child.find(metadata_ns + "Organization")

    org_names_xml = organizations.findall(metadata_ns + "OrganizationName")
    org_display_names_xml = organizations.findall(
        metadata_ns + "OrganizationDisplayName"
    )
    org_urls_xml = organizations.findall(metadata_ns + "OrganizationURL")

    contacts_xml = child.findall(metadata_ns + "ContactPerson")

    org_names = {}
    for item in org_names_xml:
        lang = item.get("{http://www.w3.org/XML/1998/namespace}lang")
        org_names[lang] = item.text

    org_display_names = {}
    for item in org_display_names_xml:
        lang = item.get("{http://www.w3.org/XML/1998/namespace}lang")
        org_display_names[lang] = item.text

    org_urls = {}
    for item in org_urls_xml:
        lang = item.get("{http://www.w3.org/XML/1998/namespace}lang")
        org_urls[lang] = item.text

    contacts = []
    for item in contacts_xml:
        contact_type = item.get("contactType")
        given_name_obj = item.find(metadata_ns + "GivenName")
        surname_obj = item.find(metadata_ns + "SurName")
        mail_obj = item.find(metadata_ns + "EmailAddress")

        given_name = ""
        surname = ""
        mail = ""

        if given_name_obj is not None:
            given_name = given_name_obj.text
        if surname_obj is not None:
            surname = surname_obj.text
        if mail_obj is not None:
            mail = mail_obj.text

        contact = {
            "contactType": contact_type,
            "GivenName": given_name,
            "SurName": surname,
            "EmailAddress": mail,
        }
        contacts.append(contact)
    idp_data = {
        "organizationNames": org_names,
        "organizationDisplayNames": org_display_names,
        "organizationUrls": org_urls,
        "contacts": contacts,
        "scopes": scopes,
    }

    res[entity_id] = idp_data

if not os.path.exists(output_dir):
    os.mkdir(output_dir)

with open(output_file, "w") as file:
    documents = yaml.dump(res, file, Dumper=IndentDumper, allow_unicode=True)
